# №1
# import math
#
# print("Введите величину угла в градусах")
# n = float(input())
# if n < 0 or n > 360:
#     print("Неверная величина угла на плоскости")
#     exit()
# print("Синус угла равен:", math.sin(math.radians(n)))
# print("Косинус угла равен:", math.cos(math.radians(n)))
# print("Тангенс угла равен:", math.tan(math.radians(n)))

# №2
# import math
#
# def NOD(a, b):
#     while a != 0 and b != 0:
#         if a > b:
#             a %= b
#         else:
#             b %= a
#     return a + b
#
# print("Введите два натуральных числа")
# try:
#     a = int(input())
#     b = int(input())
# except:
#     print("Вы ввели не натуральное число")
#     exit()
# if a<0 or b<0:
#     print("Вы ввели не натуральное число")
#     exit()
# print("НОД чисел", a, "и", b, "равен", NOD(a, b))
# print("НОД чисел", a, "и", b, "равен", math.gcd(a, b))

# №3
# from datetime import datetime
#
# print("Введите дату отправления поезда в формате ДД/ММ/ГГ ЧЧ:ММ")
# try:
#     start_date = datetime.strptime(input(), "%d/%m/%y %H:%M")
# except:
#     print("Дата введена в неверном формате")
#     exit()
# print("Введите дату прибытия поезда в формате ДД/ММ/ГГ ЧЧ:ММ")
# try:
#     end_date = datetime.strptime(input(), "%d/%m/%y %H:%M")
# except:
#     print("Дата введена в неверном формате")
#     exit()
# arrival_date = end_date - start_date
# print("Время в пути:", arrival_date)

# №4
# from datetime import datetime, timedelta
#
# print("Введите дату рождения в формате ДД/ММ/ГГ")
# try:
#     dr = datetime.strptime(input(), "%d/%m/%y")
# except:
#     print("Неверный формат ввода")
#     exit()
# day1 = timedelta(days=10000)
# min2 = timedelta(minutes=1000000)
# sec3 = timedelta(seconds=1000000000)
# print("10000 дней вам исполнится", dr + day1)
# print("1000000 минут вам исполнится", dr + min2)
# print("1000000000 секунд вам исполнится", dr + sec3)
