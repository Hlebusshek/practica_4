import random

n = random.randint(4, 30)
while n > 1:
    print("В куче", n, "камней")
    print("()" * n)
    print("Возьмите из кучи 1, 2 или 3 камня")
    while True:
        try:
            a = int(input())
            if 1 <= a <= 3 and n - a >= 1:
                break
            print("Вы ввели неверное число камней. Повторите ввод")
        except:
            print("Вы ввели неверное число камней. Повторите ввод")
    n -= a
    print("Осталось", n, "камней")
    print("()" * n)
    if n == 1:
        print("Вы выиграли!")
        exit()
    if 2 <= n <= 4:
        print("Ход программы:", n - 1, "камней из кучи взято")
        print("()")
        print("Вы проиграли")
        exit()
    if 6 <= n <= 8:
        print("Ход программы:", n - 5, "камней из кучи взято")
        n = 5
        print("Ваш ход")
        continue
    if n >= 9 or n == 5:
        b = random.randint(1, 3)
        print("Ход программы:", b, "камней из кучи взято")
        n -= b
        print("Ваш ход")
print("Конец игры")
